# Rack UI for ChucK
v0.2

![Seven ASCII-art oscillators on a rack with pretty colors.](design/preview.png)

Visual components to pretty up you ChucK development or livecoding!

*This is very much an alpha, there are few components in the modules. Additionally, it has only been tested in the VSCodium terminal and Gnome Terminal (which doesn't like my racks as much).*

You can check it out by running `chuck initialize.ck` in this folder.

This command will load dependencies then build a rack and start it. Not breaking news musically, but it's pretty fun to play around with it!

## Playing around

You might want to take a look at the score file where the Rack setup is
happening. I didn't go with something too simple as the preview is supposed to show off a little, but here's a basic example:

```js
// supposing you have a oscillator ready called osc1

// you can grab a theme from the theme bank (or make one youself)
// you can look them up in the presets folder
ThemeBank themes;

// basic oscillator module displaying its freq, period, phase and sync props
ThinSin sinM("osc1", themes.RUBY, osc1);

// Let's put our module in a Rack
Rack rack(themes.DARK);

// arrange() takes a matrix to help you display your modules on different rows
rack.arrange([[ sinM ]]);

// start the rack with a refresh rate (and an optional safe mode to get
// plain ascii instead of fancy ansi and unicode)
spork ~ rack.start(100::ms);

// you're done! just start your score.
```

## Module sizing

In order for the modules to play well together, I have to define standard sizes
(and try to obey them!). Accounting for:

- Smallest possible size (in chars): 10x8. Anything less is bound to be an
  impossible squeeze.
- No more than h*3 to fit at least 2 shelves on regular screen.

```
╭────────╮╭──────────────────╮╭────────────────────────────╮╭──────────────────╮
│ MINI   ││      HALF        ││            WIDE            ││      TOWER       │
│ 10x8   ││      20x8        ││            30x8            ││      30x24       │
│        ││                  ││                            ││                  │
│        ││                  ││                            ││                  │
│        ││                  ││                            ││                  │
│        ││                  ││                            ││                  │
╰────────╯╰──────────────────╯╰────────────────────────────╯│                  │
╭────────╮╭──────────────────╮╭────────────────────────────╮│                  │
│ THIN   ││      MEDIUM      ││            FULL            ││                  │
│ 10x16  ││      20x16       ││            30x16           ││                  │
│        ││                  ││                            ││                  │
│        ││                  ││                            ││                  │
│        ││                  ││                            ││                  │
│        ││                  ││                            ││                  │
│        ││                  ││                            ││                  │
│        ││                  ││                            ││                  │
│        ││                  ││                            ││                  │
│        ││                  ││                            ││                  │
│        ││                  ││                            ││                  │
│        ││                  ││                            ││                  │
│        ││                  ││                            ││                  │
│        ││                  ││                            ││                  │
╰────────╯╰──────────────────╯╰────────────────────────────╯╰──────────────────╯
```

Naming convention: HalfLFO, FullGain, MiniOsc...

## Roadmap to 0.3

- [ ] Modules that are actually useful? Tracking the period of a regular
  oscillator doesn't make sense, and neither does using 20 to 20k freq for an
  LFO
  - [ ] LFO with rate led, waveform detection and sync
  - [ ] osc with freq band, waveform detection, sync and optional width
  - [ ] different size of gain modules
  - [ ] keep the existing modules somewhere as… demo?
- [x]standard module sizes and according file structure
- [ ] example organization instead of eeaao
- [ ] A Rack that handles properly modules of different sizes

## Roadmap to 1
- [ ] useful modules covering the basic library
- [ ] io stickers that show cabling
- [ ] rack up the ChucK examples as a demo
- [ ] `ModuleBank` in presets for modules commonly set up together
- [ ] control list module to display keyboard control

Something else? Have fun!

-- Kay
