
public class Rack
{
    Module grid[][];

    Template tpl;
    Ansi ansi;
    Theme colors;

    // sizes
    10 =>  int MW;
    8 => int MH;

    [MW,   MH] @=> int MINI[];
    [MW*2, MH] @=> int HALF[];
    [MW*3, MH] @=> int WIDE[];
    [MW,   MH*2] @=> int THIN[];
    [MW*2, MH*2] @=> int MEDIUM[];
    [MW*3, MH*2] @=> int FULL[];
    [MW*3, MH*3] @=> int TOWER[];

    fun Rack() { Rack((new ThemeBank).DARK); }
    fun Rack(Theme colors)
    {
        colors @=> this.colors;
    }

    fun arrange(Module grid[][])
    {
        grid @=> this.grid;
    }

    fun RackSpace space(int width)
    {
        return RackSpace s(width * MW, colors);
    }

    fun string[] vmInfo()
    {
        Machine.shreds() @=> int shreds[];
        Math.min(10, shreds.size()) => int displayCount;
        string list;

        for (0 => int i; i < displayCount; i++)
        {
            shreds[i] +=> list;

            if (i < displayCount - 1) ", " +=> list;
            else if (shreds.size() > 10) "..." +=> list;
        }

        return [
            "",
            "Rack id=" + me.id() + ", " +
                Machine.numShreds() + " shreds currently running: " + list
        ];
    }

    fun void display()
    {
        display(false);
    }

    fun void display(int safe)
    {
        string lines[];

        for (Module row[]: grid)
        {
            string rowLines[];

            for (Module module: row)
            {
                if (rowLines == null) module.render(safe) @=> rowLines;
                
                else tpl.stitch(module.render(safe), rowLines) @=> rowLines;
            }
            
            if (lines == null) rowLines @=> lines;
            
            else tpl.append(rowLines, lines) @=> lines;
        }

        tpl.append(vmInfo(), lines) @=> lines;

        if (!safe) <<< lines => tpl.joinLines => ansi.clear, "" >>>;

        else <<< lines => tpl.joinLines, "" >>>;
    }

    fun void start(dur rate)
    {
        start(rate, false);
    }

    fun void start(dur rate, int safe)
    {
        while (true)
        {
            this.display(safe);

            rate => now;
        }
    }
}
