// led that blinks on time (hopefully)
public class BlinkLight extends Light
{
    false => showOff;

    fun BlinkLight(string prop, string ref)
    {
        Light(prop, ref);
    }

    fun string[] render(int safe, dur pace)
    {
        return render(safe, pace - (now % pace) <= 2::ms);
    }
}
