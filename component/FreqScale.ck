// frequency log band display ╵╵┴╵╵╵
public class FreqScale extends Component
{
    Ansi ansi;

    fun FreqScale(string prop, string ref)
    {
        Component(prop, ref);
    }
    
    fun string[] render(int safe, float f)
    {
        string component;
        ref.length() => int width;

        logScale(f, 20, 20000, width) => Std.ftoi => int pos;

        for (0 => int i; i < width; i++)
        {
            if (pos == i)
                tiles.get("freq_cursor", safe) +=> component;

            else if (i == 0)
                tiles.get("freq_start", safe) +=> component;

            else if (i == width - 1)
                tiles.get("freq_end", safe) +=> component;
            
            else tiles.get("freq_step", safe) +=> component;
        }

        if (!safe)
            ansi.mark(component, colors.get(2), colors.get(0))
                => component;
        
        return [ component ];
    }
}
