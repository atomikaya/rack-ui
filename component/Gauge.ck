// gauge
public class Gauge extends Component
{
    float min;
    float max;

    Ansi ansi;

    fun Gauge(string prop, string ref)
    {
        Component(prop, ref);
    }

    fun void setup(float min, float max)
    {
        min => this.min;
        max => this.max;
    }
    
    fun string[] render(int safe, float f)
    {
        string component;
        tiles.get("gauge_full", safe) => string full;

        if (!safe) ansi.mark(full, colors.get(1), colors.get(0)) => full;

        scale(f - min, max - min, ref.length()) => Std.ftoi => int pos;

        for (0 => int i; i < ref.length(); i++)
            (i <= pos - 1 ? full : tiles.get( "gauge_empty", safe)) +=> component;

        return [ component ];
    }
}
