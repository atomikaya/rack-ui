// resizable text
public class Label extends Component
{
    Template tpl;
    Ansi ansi;

    fun Label(string prop, string ref)
    {
        Component(prop, ref);
    }
    
    fun string[] render(int safe, string text)
    {
        tpl.resize(text, ref.length(), tiles.get("label_padding", safe))
            => string label;

        if (!safe)
            ansi.mark(label, colors.get(1), colors.get(0)) => label;
        
        return [label];
    }
}
