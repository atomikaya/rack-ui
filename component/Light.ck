// on / off led based on test given at rendering
public class Light extends Component
{
    true => int showOff;

    Ansi ansi;

    fun Light(string prop, string ref)
    {
        Component(prop, ref);
    }

    fun void setup(int showOff)
    {
        showOff => this.showOff;
    }

    fun string[] render(int safe, int turnOn)
    {
        turnOn
            ? tiles.get("light_on", safe)
            : showOff ? tiles.get("light_off", safe) : " "
            => string led;

        if (!safe && turnOn)
            ansi.mark(led, colors.get(3), colors.get(0)) => led;
        
        return [ led ];
    }
}
