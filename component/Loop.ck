// gauge
public class Loop extends Component
{
    float max;

    Ansi ansi;

    fun Loop(string prop, string ref)
    {
        Component(prop, ref);
    }

    fun void setup(float max)
    {
        max => this.max;
    }

    fun string drawPos(int pos, int n)
    {
        return pos == n ? "cursor" : "step";
    }
    
    fun string[] render(int safe, float f)
    {
        string component[3];
        // ╭○─○╮ <- starts on the top right
        // ○   ○ v- goes around the clock
        // ╰○─○╯

        scale(f, max, ref.length()) => Std.ftoi => int pos;
        [
            "topleft", drawPos(pos, 5), "straight", drawPos(pos, 0), "topright",
            drawPos(pos, 4), " ", " ", " ", drawPos(pos, 1),
            "bottomleft", drawPos(pos, 3), "straight", drawPos(pos, 2), "bottomright",
        ] @=> string layout[];

        for (0 => int i; i < layout.size(); i++)
        {
            if (layout[i] == " ") " " +=> component[i / 5];
            
            else if (!safe && layout[i] == "cursor")
                ansi.mark(
                    tiles.get("loop_" + layout[i], safe),
                    colors.get(3),
                    colors.get(0))
                +=> component[i / 5];
            
            else tiles.get("loop_" + layout[i], safe) +=> component[i / 5];
        }

        return component;
    }
}
