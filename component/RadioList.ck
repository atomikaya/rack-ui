// radio menu
public class RadioList extends Component
{
    string options[];

    Template tpl;
    Ansi ansi;

    fun RadioList(string prop, string ref)
    {
        Component(prop, ref);
    }

    fun void setup(string options[])
    {
        options @=> this.options;
    }

    fun string[] render(int safe, int choice)
    {
        string component[options.size()];

        for (0 => int i; i < options.size(); i++)
        {
            tpl.resize(options[i], ref.length() - 1, " ", 2)
                + tiles.get(choice == i
                    ? "radio_selected" : "radio_item", safe)
                => component[i];
            
            if (!safe && choice == i)
                ansi.mark(component[i], colors.get(1), colors.get(0))
                    => component[i];
        }
        
        return component;
    }
}
