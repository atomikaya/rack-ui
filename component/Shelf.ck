// rack shelf, filler component for RackSpace
public class Shelf extends Component
{
    fun Shelf(string prop, string ref)
    {
        Component(prop, ref);
    }

    fun string[] render(int safe)
    {
        return [ tiles.get("shelf", safe) ];
    }
}
