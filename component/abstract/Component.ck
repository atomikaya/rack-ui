// base chuck ui component
public class Component
{
    string prop;
    string ref;

    Theme colors;
    Tileset tiles;

    fun Component(string prop, string ref)
    {
        prop => this.prop;
        ref => this.ref;
    }

    fun void theme(Theme colors)
    {
        colors @=> this.colors;
    }

    fun void tileset(Tileset tiles)
    {
        tiles @=> this.tiles;
    }

    fun void setup()
    {
        <<< "Error: setup() not defined." >>>;
    }

    fun string[] render(int safe)
    {
        <<< "Error: render() not defined." >>>;

        return [""];
    }

    fun float scale(float a, float max, int steps)
    {
        return a / max * steps;
    }

    fun float logScale(float a, float min, float max, int steps)
    {
        if (a <= min) a - 1 => min;

        return scale(Math.log(a - min), Math.log(max - min), steps);
    }
}
