// load libs

for (string path: [ "Template", "Ansi" ])
    Machine.add(me.dir() + "lib/" + path + ".ck");

// load presets

for (string path: [ "Theme", "Tileset", "ThemeBank", "TileBank" ])
    Machine.add(me.dir() + "preset/" + path + ".ck");

// load components

for (string path: [
    "abstract/Component",
    "Light", "BlinkLight", "FreqScale", "Label", "RadioList", "Gauge", "Loop",
    "Shelf",
])
    Machine.add(me.dir() + "component/" + path + ".ck");

// load modules

for (string path: [
    "abstract/Module", "abstract/RackSpace",
    "thin/abstract/ThinOsc", "thin/ThinSin", "thin/ThinSqr",
    "medium/abstract/MediumOsc",
    "medium/MediumTri", "medium/MediumSaw", "medium/MediumPulse",
    "medium/MediumAmp6",
])
    Machine.add(me.dir() + "module/" + path + ".ck");

// load main class

Machine.add(me.dir() + "Rack.ck");

// load score

Machine.add(me.dir() + "score.ck");
