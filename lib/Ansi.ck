
public class Ansi // ansi color manager
{
    "ANSI" => string tag;
    "[" => string sequence;

    Template tpl;

    fun string mark(string text, string start, string end)
    {
        return tag + start + "m" + text + tag + end + "m";
    }

    fun string[] paint(string canvas[])
    {
        for (string line: canvas) line.replace(tag, sequence);

        return canvas;
    }

    fun string clear(string output)
    {
        return sequence + "2J" + output;
    }

    0 => int BLACK;
    1 => int RED;
    2 => int GREEN;
    3 => int YELLOW;
    4 => int BLUE;
    5 => int MAGENTA;
    6 => int CYAN;
    7 => int WHITE;
    30 => int FG;
    40 => int BG;
    60 => int BRIGHT;

    "0" => string STOP;
}
