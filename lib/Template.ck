
public class Template // template renderer
{
    fun string[] append(string lines[], string acc[])
    {
        string buffer[acc.size() + lines.size()];

        for (0 => int i; i < acc.size(); i++) acc[i] => buffer[i];

        for (0 => int i; i < lines.size(); i++)
            lines[i] => buffer[acc.size() + i];
        
        return buffer;
    }

    fun string[] dup(string template[])
    {
        string canvas[template.size()];

        for (0 => int i; i < template.size(); i++) template[i] => canvas[i];

        return canvas;
    }

    fun string joinLines(string lines[])
    {
        "\n" => string text;

        for (string line: lines) line + "\n" +=> text;

        return text;
    }

    fun string[] replace(string canvas[], string key, string values[])
    {
        int row;

        for (string line: canvas)
        {
            if (line.find(key) >= 0)
            {
                line.replace(key, values[row]);

                if (row < values.size() - 1) row++;
            }
        }

        return canvas;
    }

    fun string resize(string text, int length, string padding)
    {
        return resize(text, length, padding, 0);
    }

    fun string resize(string text, int length, string padding, int align)
    {
        text => string copy;

        if (copy.length() > length) copy.substring(0, length) => copy;

        length - copy.length() => int padCount;

        for (0 => int i; i < padCount; i++)
        {
            if (align == 0) (i % 2 ? padding + copy : copy + padding) => copy;
            else if (align == 1) copy + padding => copy;
            else padding + copy => copy;
        }
        
        return copy;
    }

    fun string[] shortestFirst(string list[])
    {
        int index[list.size()];

        for (0 => int i; i < list.size(); i++)
            list[i].length() => index[i];

        index.sort();

        string list2[list.size()];

        for (0 => int i; i < list.size(); i++)
        {
            for (0 => int j; j < index.size(); j++)
            {
                if (list[i].length() == index[j] && list2[j] == "")
                {
                    list[i] => list2[j];

                    break;
                }
        
            }
        }
        return list2;
    }

    fun string[] stitch(string lines[], string acc[])
    {
        Math.min(lines.size(), acc.size()) => int seamSize;

        string buffer[seamSize];

        for (0 => int i; i < seamSize; i++) acc[i] + lines[i] => buffer[i];

        return buffer;
    }
}
