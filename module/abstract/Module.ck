// base module class
public class Module
{
    string template[];
    string templateAscii[];

    Component components[];
    Theme colors;

    Template tpl;
    Ansi ansi;

    fun Module(Theme colors, Tileset tiles)
    {
        colors @=> this.colors;

        for (Component comp: components)
        {
            comp.theme(colors);
            comp.tileset(tiles);
        };
    }

    fun string[] renderProp(int safe, string ref)
    {
        <<< "Error: update() not defined.">>>;

        return [""];
    }

    fun string[] render(int safe)
    {
        string params[0][20];

        for (Component comp: components)
        {
            renderProp(safe, comp.prop) @=> params[comp.ref];
        }

        safe ? templateAscii : template => tpl.dup @=> string canvas[];

        string keys[0];

        params.getKeys(keys);

        for (auto key: tpl.shortestFirst(keys))
            tpl.replace(canvas, key, params[key]);
        
        if (!safe)
        {
            for (0 => int i; i < canvas.size(); i++)
                ansi.mark(canvas[i], colors.primary(), ansi.STOP)
                    => canvas[i];

            canvas => ansi.paint @=> canvas;
        }

        return canvas;
    }
}
