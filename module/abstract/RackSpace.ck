// watches 1 main and 6 sub gains
public class RackSpace extends Module
{
    Shelf s("shelf", "s");

    [ s ] @=> components;

    fun RackSpace(int width) { RackSpace(width, (new ThemeBank).DARK); }
    fun RackSpace(int width, Theme colors)
    {
        string t[template.size()], ta[templateAscii.size()];

        repeat(width)
        {
            for (0 => int i; i < template.size(); i++)
                template[i] +=> t[i];
            
            for (0 => int i; i < templateAscii.size(); i++)
                templateAscii[i] +=> ta[i];
        }

        t @=> template;
        ta @=> templateAscii;

        TileBank tiles;

        Module(colors, tiles.DEFAULTS);
    }

    fun string[] renderProp(int safe, string prop)
    {
        if (prop == "shelf") return s.render(safe);

        return [""];
    }

    [
        " ",
        " ",
        "s",
        "s",
        " ",
        " ",
        " ",
        " ",
        " ",
        " ",
        " ",
        " ",
        "s",
        "s",
        " ",
    ] @=> template;

    [
        " ",
        " ",
        "s",
        "s",
        " ",
        " ",
        " ",
        " ",
        " ",
        " ",
        " ",
        " ",
        "s",
        "s",
        " ",
    ] @=> templateAscii;
}
