// watches 1 main and 6 sub gains
public class MediumAmp6 extends Module
{
    string title;
    int gainOn[6];
    UGen targets[];

    Label titleLabel("title", "m$$$$$$$$$$$$$$$$$");
    Gauge gainM("main", "g$$$$$$$$$$$$$");
    Light on1("on1", "z");
    Gauge gain1("gain1", "g1$$");
    Light on2("on2", "y");
    Gauge gain2("gain2", "g2$$");
    Light on3("on3", "x");
    Gauge gain3("gain3", "g3$$");
    Light on4("on4", "w");
    Gauge gain4("gain4", "g4$$");
    Light on5("on5", "v");
    Gauge gain5("gain5", "g5$$");
    Light on6("on6", "u");
    Gauge gain6("gain6", "g6$$");

    [
        gainM, on1, gain1, on2, gain2, on3, gain3, on4, gain4,
        on5, gain5, on6, gain6, titleLabel,
    ] @=> components;
    

    fun MediumAmp6(string title) { MediumAmp6(title, (new ThemeBank).DARK); }
    fun MediumAmp6(string title, Theme colors)
    {
        title => this.title;

        for (Gauge g: [gainM, gain1, gain2, gain3, gain4, gain5, gain6])
            g.setup(0, 1);
        
        TileBank tiles;

        Module(colors, tiles.DEFAULTS);
    }

    fun void watch(UGen main, UGen g1, UGen g2)
    { watch(main, g1, g2, null, null, null, null); }

    fun void watch(UGen main, UGen g1, UGen g2, UGen g3)
    { watch(main, g1, g2, g3, null, null, null); }

    fun void watch(UGen main, UGen g1, UGen g2, UGen g3, UGen g4)
    { watch(main, g1, g2, g3, g4, null, null); }

    fun void watch(UGen main, UGen g1, UGen g2, UGen g3, UGen g4, UGen g5)
    { watch(main, g1, g2, g3, g4, g5, null); }

    fun void watch(UGen main, UGen g1, UGen g2, UGen g3, UGen g4, UGen g5, UGen g6)
    {
        [main, g1, g2, g3, g4, g5, g6] @=> targets;
    }

    fun string[] renderProp(int safe, string prop)
    {
        if (prop == "title")
            return titleLabel.render(safe, title);
        
        if (prop == "main")
            return gainM.render(safe, targets[0].gain());
        
        if (prop == "on1")
            return on1.render(safe, targets[1] != null);
        
        if (prop == "gain1")
            return gain1.render(safe, targets[1].gain());
        
        if (prop == "on2")
            return on2.render(safe, targets[2] != null);
        
        if (prop == "gain2")
            return gain2.render(safe, targets[2].gain());
        
        if (prop == "on3")
            return on3.render(safe, targets[3] != null);
        
        if (prop == "gain3")
            return gain3.render(safe, targets[3] == null ? .0 : targets[3].gain());
        
        if (prop == "on4")
            return on4.render(safe, targets[4] != null);
        
        if (prop == "gain4")
            return gain4.render(safe, targets[4] == null ? .0 : targets[4].gain());
        
        if (prop == "on5")
            return on5.render(safe, targets[5] != null);
        
        if (prop == "gain5")
            return gain5.render(safe, targets[5] == null ? .0 : targets[5].gain());
        
        if (prop == "on6")
            return on6.render(safe, targets[6] != null);
        
        if (prop == "gain6")
            return gain6.render(safe, targets[6] == null ? .0 : targets[6].gain());
        
        return [""];
    }

    [
        "╭m$$$$$$$$$$$$$$$$$╮",
        "│⊘                ⊘│",
        "│       MAIN       │",
        "│  g$$$$$$$$$$$$$  │",
        "│                  │",
        "│  GAIN 1  GAIN 2  │",
        "│  z g1$$  y g2$$  │",
        "│                  │",
        "│  GAIN 3  GAIN 4  │",
        "│  x g3$$  w g4$$  │",
        "│                  │",
        "│  GAIN 5  GAIN 6  │",
        "│  v g5$$  u g6$$  │",
        "│⊘                ⊘│",
        "╰━━━△━━━━━━━━━━▽━━━╯",
    ] @=> template;

    [
        ".m$$$$$$$$$$$$$$$$$.",
        "|·                ·|",
        "|       MAIN       |",
        "|  g$$$$$$$$$$$$$  |",
        "|                  |",
        "|  GAIN 1  GAIN 2  |",
        "|  z g1$$  y g2$$  |",
        "|                  |",
        "|  GAIN 3  GAIN 4  |",
        "|  x g3$$  w g4$$  |",
        "|                  |",
        "|  GAIN 5  GAIN 6  |",
        "|  v g5$$  u g6$$  |",
        "|·                ·|",
        "`===i==========o===´",
    ] @=> templateAscii;
}
