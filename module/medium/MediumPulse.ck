// pulse osc module class
public class MediumPulse extends MediumOsc
{
    PulseOsc target[0];

    fun MediumPulse(string title, PulseOsc osc) { MediumPulse(title, osc, (new ThemeBank).SAPPHIRE); }
    fun MediumPulse(string title, PulseOsc osc, Theme colors)
    {
        for (string prop: ["freq", "period", "phase", "sync", "width"])
            osc @=> target[prop];
        
        MediumOsc(title, colors);
    }

    fun string[] renderProp(int safe, string prop)
    {
        if (prop == "title")
            return titleLabel.render(safe, title);

        if (prop == "width")
            return waveWidth.render(safe, target[prop].width());
        
        if (prop == "period")
            return periodBlink.render(safe, target[prop].period());

        if (prop == "freq")
            return freqBand.render(safe, target[prop].freq());

        if (prop == "phase")
            return phaseLoop.render(safe, target[prop].phase());

        if (prop == "sync")
            return syncMenu.render(safe, target[prop].sync());
        
        return [""];
    }

    [
        "╭t$$$$$$$$$$$$$$$$$╮",
        "│⊘  ▝▔▘      ▝▔▘  ⊘│",
        "│   PULSE  WIDTH   │",
        "│  g$$$$$$$$$$$$$  │",
        "│                  │",
        "│  FREQ  p─PERIOD  │",
        "│  f$$$$$$$$$$$$$  │",
        "│  20         20K  │",
        "│                  │",
        "│  PHASE   SYNC    │",
        "│  z$$$$◂  s$$$$$  │",
        "│  z$$$$   s$$$$$  │",
        "│  z$$$$   s$$$$$  │",
        "│⊘                ⊘│",
        "╰━━━△━━━━━━━━━━▽━━━╯",
    ] @=> template;

    [
        ".t$$$$$$$$$$$$$$$$$.",
        "|·  ' '      ' '  ·|",
        "|   PULSE  WIDTH   |",
        "|  g$$$$$$$$$$$$$  |",
        "|                  |",
        "|  FREQ  p-PERIOD  |",
        "|  f$$$$$$$$$$$$$  |",
        "|  20         20K  |",
        "|                  |",
        "|  PHASE   SYNC    |",
        "|  z$$$$<  s$$$$$  |",
        "|  z$$$$   s$$$$$  |",
        "|  z$$$$   s$$$$$  |",
        "|·                ·|",
        "`===i==========o===´",
    ] @=> templateAscii;
}
