// tri osc module class
public class MediumSaw extends MediumOsc
{
    SawOsc target[0];

    fun MediumSaw(string title, SawOsc osc) { MediumSaw(title, osc, (new ThemeBank).JADE); }
    fun MediumSaw(string title, SawOsc osc, Theme colors)
    {
        for (string prop: ["freq", "period", "phase", "sync", "width"])
            osc @=> target[prop];
        
        MediumOsc(title, colors);
    }

    fun string[] renderProp(int safe, string prop)
    {
        if (prop == "title")
            return titleLabel.render(safe, title);

        if (prop == "width")
            return waveWidth.render(safe, target[prop].width());
        
        if (prop == "period")
            return periodBlink.render(safe, target[prop].period());

        if (prop == "freq")
            return freqBand.render(safe, target[prop].freq());

        if (prop == "phase")
            return phaseLoop.render(safe, target[prop].phase());

        if (prop == "sync")
            return syncMenu.render(safe, target[prop].sync());
        
        return [""];
    }

    [
        "╭t$$$$$$$$$$$$$$$$$╮",
        "│⊘  ◤◤        ◥◥  ⊘│",
        "│    SAW  WIDTH    │",
        "│  g$$$$$$$$$$$$$  │",
        "│                  │",
        "│  FREQ  p─PERIOD  │",
        "│  f$$$$$$$$$$$$$  │",
        "│  20         20K  │",
        "│                  │",
        "│  PHASE   SYNC    │",
        "│  z$$$$◂  s$$$$$  │",
        "│  z$$$$   s$$$$$  │",
        "│  z$$$$   s$$$$$  │",
        "│⊘                ⊘│",
        "╰━━━△━━━━━━━━━━▽━━━╯",
    ] @=> template;

    [
        ".t$$$$$$$$$$$$$$$$$.",
        "|·  ´´        ``  ·|",
        "|    SAW  WIDTH    |",
        "|  g$$$$$$$$$$$$$  |",
        "|                  |",
        "|  FREQ  p-PERIOD  |",
        "|  f$$$$$$$$$$$$$  |",
        "|  20         20K  |",
        "|                  |",
        "|  PHASE   SYNC    |",
        "|  z$$$$<  s$$$$$  |",
        "|  z$$$$   s$$$$$  |",
        "|  z$$$$   s$$$$$  |",
        "|·                ·|",
        "`===i==========o===´",
    ] @=> templateAscii;
}
