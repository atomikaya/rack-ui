// parent for the triangle, saw and pulse osc module class
public class MediumOsc extends Module
{
    string title;

    Label titleLabel("title", "t$$$$$$$$$$$$$$$$$");
    Gauge waveWidth("width", "g$$$$$$$$$$$$$");
    BlinkLight periodBlink("period", "p");
    FreqScale freqBand("freq", "f$$$$$$$$$$$$$");
    Loop phaseLoop("phase", "z$$$$");
    RadioList syncMenu("sync", "s$$$$$");

    [
        titleLabel, waveWidth, periodBlink, freqBand, phaseLoop, syncMenu
    ] @=> components;
    

    fun MediumOsc(string title, Theme colors)
    {
        title => this.title;

        waveWidth.setup(0, 1);
        phaseLoop.setup(1);
        syncMenu.setup([ "freq", "phase", "sync" ]);
        
        TileBank tiles;

        Module(colors, tiles.DEFAULTS);
    }
}
