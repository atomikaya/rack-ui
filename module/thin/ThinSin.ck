// sine osc module class
public class ThinSin extends ThinOsc
{
    SinOsc target[0];

    fun ThinSin(string title, SinOsc osc) { ThinSin(title, osc, (new ThemeBank).GOLD); }
    fun ThinSin(string title, SinOsc osc, Theme colors)
    {
        for (string prop: ["freq", "period", "phase", "sync"])
            osc @=> target[prop];

        ThinOsc(title, colors);
    }

    fun string[] renderProp(int safe, string prop)
    {
        if (prop == "title")
            return titleLabel.render(safe, title);

        if (prop == "freq")
            return freqBand.render(safe, target[prop].freq());
        
        if (prop == "period")
            return periodBeat.render(safe, target[prop].period());

        if (prop == "phase")
            return phaseSlider.render(safe, target[prop].phase());

        if (prop == "sync")
            return syncMenu.render(safe, target[prop].sync());
        
        return [""];
    }

    [
        "╭t$$$$$$$╮",
        "│⊘◡◠◡◠◡◠◡│",
        "│        │",
        "│ FREQ d │",
        "│ f$$$$$ │",
        "│        │",
        "│ PHASE  │",
        "│ ph$$$$ │",
        "│        │",
        "│ SYNC   │",
        "│ s$$$$$ │",
        "│ s$$$$$ │",
        "│ s$$$$$ │",
        "│⊘       │",
        "╰━━△━━▽━━╯",
    ] @=> template;

    [
        ".t$$$$$$$.",
        "|· SINE  |",
        "|        |",
        "| FREQ d |",
        "| f$$$$$ |",
        "|        |",
        "| PHASE  |",
        "| ph$$$$ |",
        "|        |",
        "| SYNC   |",
        "| s$$$$$ |",
        "| s$$$$$ |",
        "| s$$$$$ |",
        "|·       |",
        "`==i==o==´",
    ] @=> templateAscii;
}
