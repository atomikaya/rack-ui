// sine osc module class
public class ThinSqr extends ThinOsc
{
    SqrOsc target[0];

    fun ThinSqr(string title, SqrOsc osc) { ThinSqr(title, osc, (new ThemeBank).NEON); }
    fun ThinSqr(string title, SqrOsc osc, Theme colors)
    {
        for (string prop: ["freq", "period", "phase", "sync"])
            osc @=> target[prop];
        
        ThinOsc(title, colors);
    }

    fun string[] renderProp(int safe, string prop)
    {
        if (prop == "title")
            return titleLabel.render(safe, title);

        if (prop == "freq")
            return freqBand.render(safe, target[prop].freq());
        
        if (prop == "period")
            return periodBeat.render(safe, target[prop].period());

        if (prop == "phase")
            return phaseSlider.render(safe, target[prop].phase());

        if (prop == "sync")
            return syncMenu.render(safe, target[prop].sync());
        
        return [""];
    }

    [
        "╭t$$$$$$$╮",
        "│⊘┌┐┌┐┌┐┌│",
        "│└┘└┘└┘└┘│",
        "│ FREQ d │",
        "│ f$$$$$ │",
        "│        │",
        "│ PHASE  │",
        "│ ph$$$$ │",
        "│        │",
        "│ SYNC   │",
        "│ s$$$$$ │",
        "│ s$$$$$ │",
        "│ s$$$$$ │",
        "│⊘       │",
        "╰━━△━━▽━━╯",
    ] @=> template;

    [
        ".t$$$$$$$.",
        "|·SQUARE |",
        "|        |",
        "| FREQ d |",
        "| f$$$$$ |",
        "|        |",
        "| PHASE  |",
        "| ph$$$$ |",
        "|        |",
        "| SYNC   |",
        "| s$$$$$ |",
        "| s$$$$$ |",
        "| s$$$$$ |",
        "|·       |",
        "`==i==o==´",
    ] @=> templateAscii;
}
