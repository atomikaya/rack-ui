// basic osc module class
public class ThinOsc extends Module
{
    string title;

    Label titleLabel("title", "t$$$$$$$");
    FreqScale freqBand("freq", "f$$$$$");
    BlinkLight periodBeat("period", "d");
    Gauge phaseSlider("phase", "ph$$$$");
    RadioList syncMenu("sync", "s$$$$$");

    [
        titleLabel, freqBand, periodBeat, phaseSlider, syncMenu
    ] @=> components;

    fun ThinOsc(string title, Theme colors)
    {
        title => this.title;

        phaseSlider.setup(0, 1);
        syncMenu.setup([ "freq", "phase", "sync" ]);
        
        TileBank tiles;

        Module(colors, tiles.DEFAULTS);
    }
}
