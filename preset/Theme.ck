
public class Theme // color theme
{
    int fgPrimary;
    int fgSecondary;
    int fgFlipped;
    int fgLight;
    int bgPrimary;
    int bgSecondary;
    int bgFlipped;

    Ansi ansi;

    fun string get(int n)
    {
        return n == 1 ? secondary() : n == 2 ? flipped() : n == 3 ? lit() : primary();
    }

    fun string primary()
    {
        return bgPrimary + ";" + fgPrimary;
    }

    fun void primary(int fg, int bg)
    {
        fg + ansi.FG => this.fgPrimary;
        bg + ansi.BG => this.bgPrimary;
    }

    fun string secondary()
    {
        return bgSecondary + ";" + fgSecondary;
    }

    fun void secondary(int fg, int bg)
    {
        fg + ansi.FG => this.fgSecondary;
        bg + ansi.BG => this.bgSecondary;
    }

    fun string flipped()
    {
        return bgFlipped + ";" + fgFlipped;
    }

    fun void flipped(int fg, int bg)
    {
        fg + ansi.FG => this.fgFlipped;
        bg + ansi.BG => this.bgFlipped;
    }

    fun string lit()
    {
        return bgPrimary + ";" + fgLight;
    }

    fun void lit(int fg)
    {
        fg + ansi.FG => this.fgLight;
    }

}
