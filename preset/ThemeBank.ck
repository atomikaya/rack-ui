
public class ThemeBank
{
    Ansi ansi;

    Theme RUBY;
    RUBY.primary(ansi.BLACK, ansi.RED);
    RUBY.secondary(ansi.WHITE, ansi.RED);
    RUBY.flipped(ansi.YELLOW, ansi.BLACK);
    RUBY.lit(ansi.WHITE + ansi.BRIGHT);

    Theme SAPPHIRE;
    SAPPHIRE.primary(ansi.BLACK, ansi.BLUE);
    SAPPHIRE.secondary(ansi.WHITE, ansi.BLUE);
    SAPPHIRE.flipped(ansi.CYAN, ansi.BLACK);
    SAPPHIRE.lit(ansi.CYAN + ansi.BRIGHT);

    Theme NEON;
    NEON.primary(ansi.BLACK + ansi.BRIGHT, ansi.BLACK);
    NEON.secondary(ansi.MAGENTA, ansi.BLACK);
    NEON.flipped(ansi.BLACK, ansi.MAGENTA);
    NEON.lit(ansi.MAGENTA + ansi.BRIGHT);

    Theme GOLD;
    GOLD.primary(ansi.YELLOW, ansi.BLACK);
    GOLD.secondary(ansi.YELLOW + ansi.BRIGHT, ansi.BLACK);
    GOLD.flipped(ansi.BLACK, ansi.YELLOW + ansi.BRIGHT);
    GOLD.lit(ansi.WHITE + ansi.BRIGHT);
    
    Theme JADE;
    JADE.primary(ansi.BLACK, ansi.GREEN);
    JADE.secondary(ansi.WHITE, ansi.GREEN);
    JADE.flipped(ansi.GREEN + ansi.BRIGHT, ansi.BLACK);
    JADE.lit(ansi.WHITE + ansi.BRIGHT);

    Theme BOT;
    BOT.primary(ansi.BLACK + ansi.BRIGHT, ansi.WHITE);
    BOT.secondary(ansi.BLACK + ansi.BRIGHT, ansi.WHITE);
    BOT.flipped(ansi.BLACK, ansi.WHITE);
    BOT.lit(ansi.RED + ansi.BRIGHT);

    Theme DARK;
    DARK.primary(ansi.BLACK + ansi.BRIGHT, ansi.BLACK);
    DARK.secondary(ansi.WHITE, ansi.BLACK);
    DARK.flipped(ansi.BLACK, ansi.BLACK + ansi.BRIGHT);
    DARK.lit(ansi.GREEN + ansi.BRIGHT);

    [ RUBY, SAPPHIRE, NEON, GOLD, JADE, BOT, DARK ] @=> Theme ALL[];
}
