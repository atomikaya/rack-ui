public class TileBank
{
    Tileset DEFAULTS([
        ["label_padding", "─", "-"],
        ["freq_start", "╵", "\""],
        ["freq_step", "╵", "'"],
        ["freq_end", "╵", "\""],
        ["freq_cursor", "┻", "^"],
        ["gauge_full", "━", "="],
        ["gauge_empty", "═", "-"],
        ["radio_item", "▫", "⋅"],
        ["radio_selected", "▪", "¬"],
        ["loop_step", "○", "o"],
        ["loop_cursor", "●", "@"],
        ["loop_straight", "─", "-"],
        ["loop_topleft", "╭", "."],
        ["loop_topright", "╮", "."],
        ["loop_bottomleft", "╰", "`"],
        ["loop_bottomright", "╯", "´"],
        ["light_on", "●", "o"],
        ["light_off", "○", "x"],
        ["shelf", "░", "H"],
    ]);
}
