public class Tileset
{
    string unicode[0];
    string ascii[0];

    fun Tileset(string tiles[][])
    {
        for (string tile[]: tiles)
        {
            if (tile.size() != 3) continue;

            tile[1] => unicode[tile[0]];
            tile[2] => ascii[tile[0]];
        }
    }

    fun string get(string key, int safe)
    {
        string keys[];

        if (!(key => unicode.isInMap))
        {
            <<<  "Error: tile not found." >>>;
            
            return "";
        }

        return safe ? ascii[key] : unicode[key];
    }
}
