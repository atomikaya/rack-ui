// sync

.5::second => dur beat;
beat - (now % beat) => now;

// music setup

Gain vol => dac;
SinOsc osc1 => ADSR env1 => vol;
TriOsc osc2 => ADSR env2 => vol;
SawOsc osc3 => ADSR env3 => vol;
PulseOsc osc4 => ADSR env4 => vol;
SqrOsc osc5 => ADSR env5 => vol;

.5 => vol.gain; // sum of oscs must add up to 1 after vol

.7 => osc1.gain;
.5 => osc2.gain;
.3 => osc3.gain;
.3 => osc4.gain;
.2 => osc5.gain;

220 => osc3.freq;
330 => osc4.freq;
.234 => osc4.width;

env1.set(10::ms, 300::ms, .2, 10::ms);
env2.set(500::ms, 500::ms, .4, 100::ms);
env3.set(10::ms, 300::ms, .2, 10::ms);
env4.set(10::ms, 300::ms, .2, 10::ms);
env4.set(500::ms, 300::ms, .2, 10::ms);

// rack setup

ThemeBank themes;

ThinSin sinM("osc1", themes.GOLD, osc1);
MediumTri triM("osc2", themes.RUBY, osc2);
MediumSaw sawM("osc3", themes.JADE, osc3);
MediumPulse pulseM("osc4", themes.SAPPHIRE, osc4);
ThinSqr sqrM("osc5", themes.NEON, osc5);

MediumAmp6 ampM("mix", themes.DARK);

ampM.watch(vol, osc1, osc2, osc3, osc4, osc5);

Rack rack(themes.DARK);

rack.arrange([
    [ sinM, rack.space(1), ampM, rack.space(1), sqrM ],
    [ triM, sawM, pulseM ],
]);

spork ~ rack.start(100::ms);

// score

while (true)
{
    Math.random2(0, 2) => int target;

    if (target == 1)
    {
        Math.random2(12*3, 12*6) => Std.mtof => osc1.freq => osc5.freq;
        true => env1.keyOn;
        true => env5.keyOn;
    }
    
    else if (target == 2) {
        Math.random2(0, 1) * 1.0 => osc3.width;
        true => env3.keyOn;
        true => env4.keyOn;
    }

    else {
        osc2.freq() => Std.ftom => Std.ftoi => int note;
        Math.clampi(Math.random2(-3, 3) + note, 12*3, 12*6)
            => Std.mtof => osc2.freq;
        true => env2.keyOn;
    }

    beat => now;

    for (ADSR env: [env1, env2, env3, env4, env5])
        true => env.keyOff;
}
